<?php

namespace App\Test\TestCase\Controller;

use App\Controller\DemoController;
use Cake\TestSuite\TestCase;
/**
 * PagesControllerTest class
 */
class DemoControllerTest extends TestCase {
    
    /**
     * @test
     */
    function indexTest(){
        $demo=new DemoController();
        $this->assertEquals(3, $demo->index(1, 2));
    }
}
